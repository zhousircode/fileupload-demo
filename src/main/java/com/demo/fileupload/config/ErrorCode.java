package com.demo.fileupload.config;

/**
 * Created by lixiaoxin on 2017/12/8.
 */
public interface ErrorCode {
    int ERROR = 4001;
    int ILLEGAL_ARGUMENT_ERROR = 4002;
}
