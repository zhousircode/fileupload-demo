package com.demo.fileupload.repository;

import com.demo.fileupload.entity.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lixiaoxin on 2017/12/15.
 */
@Repository
public interface ResourceRepository extends JpaRepository<Resource, Long> {
    Resource getById(long lid);

    Page<Resource> findAll(Pageable pageable);

}
