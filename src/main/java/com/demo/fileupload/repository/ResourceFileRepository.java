package com.demo.fileupload.repository;

import com.demo.fileupload.entity.ResourceFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lixiaoxin on 2017/12/15.
 */
@Repository
public interface ResourceFileRepository extends JpaRepository<ResourceFile, Long> {
    ResourceFile findFirstByMd5Value(String md5Value);

    ResourceFile getById(Long id);
}
