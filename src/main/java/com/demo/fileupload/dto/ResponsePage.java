package com.demo.fileupload.dto;

/**
 * Created by lixiaoxin on 2017/12/6.
 */
public class ResponsePage {
    private int current_page;
    private int page_count;
    private int per_page;
    private long count;

    public ResponsePage() {
    }

    public ResponsePage(int current_page, int page_count, int per_page, int count) {
        this.current_page = current_page;
        this.page_count = page_count;
        this.per_page = per_page;
        this.count = count;
    }

    public int getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public int getPage_count() {
        return page_count;
    }

    public void setPage_count(int page_count) {
        this.page_count = page_count;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
